import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Paciente } from 'src/app/_model/paciente';
import { PacienteService } from 'src/app/_service/paciente.service';
import { map, switchMap } from 'rxjs';
import { MatDialogRef } from '@angular/material/dialog';
import { SignosService } from 'src/app/_service/signos.service';

@Component({
  selector: 'app-paciente-dialogo',
  templateUrl: './paciente-dialogo.component.html',
  styleUrls: ['./paciente-dialogo.component.css']
})
export class PacienteDialogoComponent implements OnInit {

  form: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<PacienteDialogoComponent>,
    private pacienteService: PacienteService,
    private signosService: SignosService,
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      'id': new FormControl(0),
      'nombres': new FormControl('', [Validators.required, Validators.minLength(3)]),
      'apellidos': new FormControl('', [Validators.required, Validators.minLength(3)]),
      'dni': new FormControl(''),
      'direccion': new FormControl(''),
      'telefono': new FormControl(''),
      'email': new FormControl('')
    });
  }

  get f() { return this.form.controls; }

  guardar() {
    if(this.form.invalid){ return; }

    let paciente = new Paciente();
    paciente.idPaciente = this.form.value['id'];
    paciente.nombres = this.form.value['nombres'];
    paciente.apellidos = this.form.value['apellidos'];
    paciente.dni = this.form.value['dni'];
    paciente.direccion = this.form.value['direccion'];
    paciente.telefono = this.form.value['telefono'];
    paciente.email = this.form.value['email'];

    this.pacienteService.registrar(paciente).subscribe((data) => {
      this.signosService.setMensajeCambio('Se registró un nuevo Paciente');
      this.dialogRef.close(true);
    });

  }

}
