import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { map, Observable } from 'rxjs';
import { Paciente } from 'src/app/_model/paciente';
import { Signos } from 'src/app/_model/signos';
import { PacienteService } from 'src/app/_service/paciente.service';
import { SignosService } from 'src/app/_service/signos.service';
import { switchMap } from 'rxjs/operators';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as moment from 'moment';
import { MatDialog } from '@angular/material/dialog';
import { PacienteDialogoComponent } from '../paciente-dialogo/paciente-dialogo/paciente-dialogo.component';

@Component({
  selector: 'app-signos-edicion',
  templateUrl: './signos-edicion.component.html',
  styleUrls: ['./signos-edicion.component.css']
})
export class SignosEdicionComponent implements OnInit {

  formSignos: FormGroup;

  //utiles para el autocomplete
  myControlPaciente: FormControl = new FormControl('', Validators.required);

  pacientes: Paciente[];
  pacientesFiltrados$: Observable<Paciente[]>;

  //#region variables para formulario
  id: number;
  edicion: boolean = false;
  maxFecha: Date = new Date();
  //#endregion

  constructor(
    private signosService: SignosService,
    private pacienteService: PacienteService,
    private route: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.formSignos = new FormGroup({
      'id': new FormControl(0),
      'paciente': this.myControlPaciente,
      'fecha': new FormControl(new Date()),
      'temperatura': new FormControl(''),
      'pulso': new FormControl(''),
      'ritmoRespiratorio': new FormControl('')
    });

    this.listarPacientes();

    this.pacientesFiltrados$ = this.myControlPaciente.valueChanges.pipe(map(val => this.filtrarPacientes(val)));

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });
    
  }

  initForm() {
    if (this.edicion) {
      this.signosService.listarPorId(this.id).subscribe(data => {
        this.myControlPaciente.setValue(data.paciente);
        this.formSignos = new FormGroup({
          'id': new FormControl(data.idSignos),
          'paciente': this.myControlPaciente,
          'fecha': new FormControl(new Date(data.fecha)),
          'temperatura': new FormControl(data.temperatura),
          'pulso': new FormControl(data.pulso),
          'ritmoRespiratorio': new FormControl(data.ritmoRespiratorio)
        });
        
      });
    }
  }

  listarPacientes(seleccionarUltimo: boolean = false) {
    this.pacienteService.listar().subscribe(data => {
      this.pacientes = data;
      
      if(seleccionarUltimo){
        let pacientesTemporal = data;
        pacientesTemporal.sort((a, b) => b.idPaciente - a.idPaciente);

        if(pacientesTemporal.length > 0){
          let ultimoPaciente = pacientesTemporal[0];
          this.myControlPaciente.setValue(ultimoPaciente);
        }
      }
    });
  }

  mostrarPaciente(val: any) {
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }

  filtrarPacientes(val: any) {
    return this.pacientes.filter(el =>
      el.nombres.toLowerCase().includes(val) || el.apellidos.toLowerCase().includes(val) || el.dni.includes(val)
    );
  }

  guardar() {

    let signos = new Signos();
    signos.idSignos = this.formSignos.value['id'];
    signos.paciente = this.formSignos.value['paciente'];
    signos.fecha = moment(this.formSignos.value['fecha']).format('YYYY-MM-DDTHH:mm:ss');
    signos.temperatura = this.formSignos.value['temperatura'];
    signos.pulso = this.formSignos.value['pulso'];
    signos.ritmoRespiratorio = this.formSignos.value['ritmoRespiratorio'];

    if (signos.idSignos > 0) {
      this.signosService.modificar(signos).pipe(switchMap(() => {
        return this.signosService.listar();
      })).subscribe(data => {
        this.signosService.setSignosCambio(data);
        this.signosService.setMensajeCambio("El signo se modificó correctamente");
      });
    } else {
      this.signosService.registrar(signos).pipe(switchMap(() => {
        return this.signosService.listar();
      })).subscribe(data => {
        this.signosService.setSignosCambio(data);
        this.signosService.setMensajeCambio("El signo se registró correctamente");
      });
    }

    this.router.navigate(['/pages/signos']);
  }

  abrirDialogoPaciente(){
    const dialogRef = this.dialog.open(PacienteDialogoComponent, {
      width: '400px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.listarPacientes(true);
      }
    });
  }

}
