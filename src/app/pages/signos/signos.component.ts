import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs';
import { Signos } from 'src/app/_model/signos';
import { AlertService } from 'src/app/_service/alert.service';
import { SignosService } from 'src/app/_service/signos.service';

@Component({
  selector: 'app-signos',
  templateUrl: './signos.component.html',
  styleUrls: ['./signos.component.css']
})
export class SignosComponent implements OnInit {

  displayedColumns = ['id', 'paciente', 'fecha', 'temperatura', 'pulso', 'ritmoRespiratorio', 'acciones'];
  dataSource: MatTableDataSource<Signos>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private signosService: SignosService,
    private alertService: AlertService
  ) { }

  ngOnInit(): void {
    this.signosService.getSignosCambio().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });

    this.signosService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'Aviso', {
        duration: 2000,
      });
    });

    this.signosService.listar().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.dataSource.filterPredicate = function customFilter(data , filter:string ): boolean {
          let nombreCompleto = `${data.paciente.nombres} ${data.paciente.apellidos}`.trim().toLocaleLowerCase();
          return (
            nombreCompleto.includes(filter) ||
            data.temperatura.toString().includes(filter) ||
            data.pulso.toString().includes(filter) ||
            data.ritmoRespiratorio.toString().includes(filter)
          );
      }
    });
  }

  filtrar(e: any) {
    this.dataSource.filter = e.target.value.trim().toLowerCase();
  }

  verificarHijos(){
    return this.route.children.length !== 0
  }

  eliminar(signo: Signos) {

    this.alertService.confirm(
      'Esta seguro que desea eliminar este signo?'
    ).then((result) => {
      if (result.value) {
        this.signosService.eliminar(signo.idSignos).pipe(switchMap(() => {
          return this.signosService.listar();
        })).subscribe(data => {
          this.signosService.setSignosCambio(data);
          this.signosService.setMensajeCambio('Se eliminó el registro');
        });
      }
    });
  }

}
