import { Paciente } from "./paciente";

export class Signos {
    idSignos: number;
    paciente: Paciente;
    fecha: string;
    temperatura: number;
    pulso: number;
    ritmoRespiratorio: number;
}
